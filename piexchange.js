/** Import packages */
var express = require('express')
var exphbs  = require('express-handlebars');
var mongoose = require('mongoose');
mongoose.Promise = require('q').Promise;

var app = express()

app.engine('handlebars', exphbs({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');
app.use(express.static(__dirname + '/public'));
app.set('port', process.env.PORT || 3000);

/** local database */
var Admin = require('./models/admin.js');
var Client = require('./models/client.js');
var Account = require('./models/account.js');

/** Seeds */
require('./seeds.js')(Admin, Client, Account);

/** Database configuration */
mongoose.connect('mongodb://localhost/test');

app.get('/', function (req, res) {
    res.render('home');
});

app.get('/modify', function (req, res) {
	res.type('text/plain');
    res.send('Data changed.');
});

app.get('/display', function (req, res) {
	
	Account.find(function (err, accounts) {
		if (err) return console.error(err);
		console.log(accounts);
	})

	Client.find(function (err, clients) {
		if (err) return console.error(err);
		console.log(clients);
	})

    res.type('text/plain');
    res.send('Check the console.');
});

app.listen(app.get('port'), function () {
	console.log( 'Express started in ' + app.get('env') + ' mode on http://localhost:' + 
		app.get('port') + '; press Ctrl-C to terminate.' );
});