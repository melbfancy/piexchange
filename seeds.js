module.exports = function (Admin, Client, Account){

	/** Clear database
	Account.remove({}, function(err){
		if (err)
			throw new Error('Fail to clear database');
	});

	Client.remove({}, function(err){
		if (err)
			throw new Error('Fail to clear database');
	});

	Admin.remove({}, function(err){
		if (err)
			throw new Error('Fail to clear database');
	});
	*/

	/** Initial seeds

	account1 = new Account({
		login: 'account1',
		password: '12345678'
	});

	account1.save(function (err) {
		if (err) return console.error(err);
	});

	account2 = new Account({
		login: 'account2',
		password: '12345678'
	});

	account2.save(function (err) {
		if (err) return console.error(err);
	});

	client1 = new Client({
		name: 'client1',
		clientType: 'individual'
	});

	client1.save(function (err) {
		if (err) return console.error(err);
	});

	client2 = new Client({
		name: 'client2',
		clientType: 'organisation'
	});

	client2.save(function (err) {
		if (err) return console.error(err);
	});
	*/

	/** Relations
	var foundClient;

	Client.findOne({ name: 'client1' })
	.then(function(client){
		foundClient = client;
		return Account.findOne({login: 'account1'});
	})
	.then(function(account1){
		foundClient.accounts.push(account1);
		return foundClient.save();
	})
	.catch(function(err){
		console.error(err)
	})
	*/
}