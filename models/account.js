/**
 * Account model
 */
var mongoose = require("mongoose");

var accountSchema = mongoose.Schema({
    login: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true,
        minlength: 8
    },
    status: String
});

var Account = mongoose.model('Account', accountSchema);
module.exports = Account;
