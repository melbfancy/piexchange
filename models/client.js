/**
 * Client model
 */
var mongoose = require("mongoose");

var clientSchema = mongoose.Schema({
    name: {
    	type: String,
    	required: true
    },
    status: String,
    clientType: {
    	type: String,
    	enum: ['individual', 'organisation']
    },
    accounts: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Account' }]
});

clientSchema.pre('save', function(next) {
	if(this.clientType == 'individual' && this.accounts.length > 1) {
		var err = new Error('Only one account is allowed to be created for an individual client');
		next(err);
	}
	next();
});

var Client = mongoose.model('Client', clientSchema);
module.exports = Client;
