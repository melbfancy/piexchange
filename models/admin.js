/**
 * Admin model
 */
var mongoose = require("mongoose");

var adminSchema = mongoose.Schema({
    name: {
    	type: String,
    	required: true
    },
    password: {
    	type: String,
    	required: true,
    	minlength: 8
    }
});

var Admin = mongoose.model('Admin', adminSchema);
module.exports = Admin;
